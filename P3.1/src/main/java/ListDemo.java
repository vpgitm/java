import java.util.*;

public class ListDemo {
    public static ArrayList<String> stringsWithIncludedChar(List<String> stringList, char letter){
        ArrayList<String> stringListResult=new ArrayList<String>();
        for (String string:stringList) {
            if(string.charAt(0)==letter){
                stringListResult.add(string);
            }
        }
        return stringListResult;
    }

    public static ArrayList<Human> excludeHuman(List<Human> stringList, Human excludedHuman ){
        ArrayList<Human> humanListResult=new ArrayList<Human>();
        for (Human human:stringList) {
            if( ! excludedHuman.equals(human) ){
                humanListResult.add(human);
            }
        }
        return humanListResult;
    }
    public static  ArrayList<HashSet<Integer>> setsWithoutIntersection(List<HashSet<Integer>> integerSets, Set<Integer> intersectioned){
        ArrayList<HashSet<Integer>> listofSetofIntegerResult = new  ArrayList<HashSet<Integer>>();
        boolean isIntersectioned;
        for (HashSet<Integer> set: integerSets){
            isIntersectioned=false;
            for(Integer i: set){
                if(intersectioned.contains(i)){
                    isIntersectioned=true;
                    break;                                                        // Проверить
                }
            }
            if(isIntersectioned==false){
                listofSetofIntegerResult.add(set);
            }
        }
        return listofSetofIntegerResult;
    }

    public static ArrayList<Human> selectionByIdentifier(Map<Integer,Human> map, Set<Integer> set){
        ArrayList<Human> selectionResult= new ArrayList<Human>();
        for (Integer key:set){
            if(map.containsKey(key)){
                selectionResult.add(map.get(key));
            }
        }
        return selectionResult;
    }

    public static HashMap<Integer,Human> olderThen18 (List<Human> list){
        HashMap<Integer, Human> olderThen18=new HashMap<Integer, Human>() {
        };
        int i=1;
        for (Human human:list) {
            if(human.getAge()>=18){
                olderThen18.put(i,human);
                i++;
            }
        }
        return olderThen18;
    }
}
