import java.io.IOException;



public class Human {
    String firstName;
    String secondName;
    String lastName;
    int age;
    private void check(String firstName, String secondName, String lastName, int age)throws IOException{
        if(firstName==null||secondName==null||lastName==null||age<0){
            throw new IOException();
        }
        return;
    }
    public Human(String firstName, String secondName, String lastName, int age) throws IOException{
        check(firstName,secondName,lastName,age);
        this.firstName = firstName;
        this.secondName = secondName;
        this.lastName = lastName;
        this.age = age;
    }

    public void setFirstName(String firstName) throws IOException {
        if(firstName==null){
            throw new IOException();
        }
        this.firstName = firstName;
    }

    public void setSecondName(String secondName) throws IOException {
        if(secondName==null){
            throw new IOException();
        }
        this.secondName = secondName;
    }

    public void setLastName(String lastName) throws IOException {
        if(lastName==null){
            throw new IOException();
        }
        this.lastName = lastName;
    }

    public void setAge(int age) throws IOException {
        if(age<0){
            throw new IOException();
        }
        this.age = age;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Human)) return false;

        Human human = (Human) o;

        if (getAge() != human.getAge()) return false;
        if (!getFirstName().equals(human.getFirstName())) return false;
        if (!getSecondName().equals(human.getSecondName())) return false;
        return getLastName().equals(human.getLastName());
    }

    @Override
    public int hashCode() {
        int result = getFirstName().hashCode();
        result = 31 * result + getSecondName().hashCode();
        result = 31 * result + getLastName().hashCode();
        result = 31 * result + getAge();
        return result;
    }
}
