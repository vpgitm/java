import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class ReflectionDemo {
    private static boolean isPresentClass(Object object, Class aClass){
        Class currentClass=object.getClass();
        while(currentClass!=null){
            if(currentClass==aClass){
                return true;
            }
            currentClass=currentClass.getSuperclass();
        }
        return false;
    }
 public static int countOfHumans(List<Object> list){
     int result=0;
    for(Object object: list){
        if(isPresentClass(object, Human.class)){
            result++;
        }
    }
    return result;
 }
    public static ArrayList<String> openMethods(Object object){
        Method[] publicMethods = object.getClass().getMethods();
        ArrayList<String> result= new ArrayList<String>();
        for (Method method : publicMethods) {
            result.add(method.getName());
        }
        return  result;
    }
    public static ArrayList<String> getAllSuperclassesNames(Object object){
        ArrayList<String> result= new ArrayList<String>();
        Class currentClass=object.getClass();
        while(currentClass!=null){
            result.add(currentClass.getName());
            currentClass=currentClass.getSuperclass();
        }
        return result;
    }
}
