import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;


public class ReflectionDemoTest {
    @Test
    public void countOfHumansTest() throws Exception {
        ArrayList<Object> list = new ArrayList<Object>();
        list.add(1);
        list.add("Qwerty");
        list.add(new Human("Felix", "Edmundovich", "Dzerzhinsky",48));
        list.add(new Human("John","","Doe",16));

        int expected = 2;
        int actual=ReflectionDemo.countOfHumans(list);
        Assert.assertEquals(expected,actual);
    }

    @Test
    public void openMethodsTest() throws Exception {
        ArrayList<String> actual = new ArrayList<String>();
        ArrayList<String> expected = new ArrayList<String>();
        expected.add("wait");
        expected.add("wait");
        expected.add("wait");
        expected.add("equals");
        expected.add("toString");
        expected.add("hashCode");
        expected.add("getClass");
        expected.add("notify");
        expected.add("notifyAll");

        actual=ReflectionDemo.openMethods(new Object());

        Assert.assertEquals(expected,actual);
    }

    @Test
    public void getAllSuperclassesNamesTest() throws Exception {
        ArrayList<String> actual = new ArrayList<String>();
        ArrayList<String> expected = new ArrayList<String>();
        expected.add("Human");
        expected.add("java.lang.Object");

        actual=ReflectionDemo.getAllSuperclassesNames(new Human("John","","Doe",16));

        Assert.assertEquals(expected,actual);
    }

}