import org.junit.Assert;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import static org.junit.Assert.*;

class Student extends Human{
    String group;
    public Student(String firstName, String secondName, String lastName, int age,String group) throws IOException {
        super(firstName, secondName, lastName, age);
        this.group=group;
    }
}
public class ListDemoTest {

    @org.junit.Test
    public void stringsWithIncludedCharTest() throws Exception {
        ArrayList<String> dataList= new ArrayList<String>();
        dataList.add("Eugine");
        dataList.add("Clare");
        dataList.add("Ethan");

        ArrayList<String> expected = new ArrayList<String>();
        expected.add("Eugine");
        expected.add("Ethan");

        ArrayList<String> actual = ListDemo.stringsWithIncludedChar(dataList,'E');
        Assert.assertEquals(expected, actual);
    }

    @org.junit.Test
    public void excludeHumanTest() throws Exception {
        ArrayList<Human> dataList=new ArrayList<Human>();
        dataList.add(new Human("Felix", "Edmundovich", "Dzerzhinsky",48));
        dataList.add(new Human("John","Moses","Browning",71));
        dataList.add(new Human("Vlad","The Third","Basarab",45));
        dataList.add(new Student("Ivan","Ivanovich","Ivanov",20,"A-1"));

        ArrayList<Human> expected=new ArrayList<Human>();
        expected.add(new Human("Felix", "Edmundovich", "Dzerzhinsky",48));
        expected.add(new Human("John","Moses","Browning",71));
        expected.add(new Student("Ivan","Ivanovich","Ivanov",20,"A-1"));

        ArrayList<Human> actual=ListDemo.excludeHuman(dataList,new Human("Vlad","The Third","Basarab",45));
        Assert.assertEquals(expected, actual);
    }

    @org.junit.Test
    public void setsWithoutIntersectionTest() throws Exception {
        ArrayList<HashSet<Integer>> data = new ArrayList<HashSet<Integer>>();
        HashSet<Integer> set1= new HashSet<Integer>();
        set1.add(1);
        set1.add(2);
        set1.add(3);
        HashSet<Integer> set2= new HashSet<Integer>();
        set2.add(4);
        set2.add(5);
        set2.add(6);
        data.add(set1);
        data.add(set2);
        HashSet<Integer> set3= new HashSet<Integer>();
        set3.add(5);
        ArrayList<HashSet<Integer>> expected=new ArrayList<HashSet<Integer>>();
        expected.add(set1);


        ArrayList<HashSet<Integer>> actual=ListDemo.setsWithoutIntersection(data, set3);
        Assert.assertEquals(expected,actual);
    }

    @org.junit.Test
    public void selectionByIdentifierTest() throws Exception {
        HashSet<Integer> set= new HashSet<Integer>();
        set.add(2);
        HashMap map=new HashMap();
        map.put(1,new Human("Felix", "Edmundovich", "Dzerzhinsky",48));
        map.put(2,new Human("John","Moses","Browning",71));
        map.put(3,new Student("Ivan","Ivanovich","Ivanov",20,"A-1"));

        ArrayList<Human> actual=ListDemo.selectionByIdentifier(map, set);
        ArrayList<Human> expected = new ArrayList<Human>();
        expected.add(new Human("John","Moses","Browning",71));
        Assert.assertEquals(expected, actual);

    }

    @org.junit.Test
    public void olderThen18Test() throws Exception {
        ArrayList<Human> data=new ArrayList<Human>();
        data.add(new Human("John","Moses","Browning",71));
        data.add(new Human("John","","Doe",16));
        data.add(new Student("Ivan","Ivanovich","Ivanov",17,"A-1"));
        HashMap<Integer, Human> actual=ListDemo.olderThen18(data);
        HashMap<Integer, Human> expected=new HashMap<Integer, Human>();
        expected.put(1,new Human("John","Moses","Browning",71));
        Assert.assertEquals(expected,actual);
    }

}