
public class InsertionSort implements Sorting {
    public double[] SortArray(double[] data){
        double buff;
        int n=data.length;
        for(int i=1;i<n;i++) {
            buff=data[i];
            for (int j = i; j > 0 && data[j - 1] > data[j]; j--) {
                buff=data[j - 1];//(data[j-1],data[j]);
                data[j - 1]=data[j];
                data[j]=data[j - 1];
            }
        }
        return data;
    }
}
