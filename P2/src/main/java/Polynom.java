class NotTwoRoots extends Exception{  //Zero for infinite amount
    public NotTwoRoots() {}
    public NotTwoRoots(int amountofroots) {
    }
}

public class Polynom {
    double a,b,c;
    double[] poly=new double[3];
    public Polynom(double the_a,double the_b,double the_c){
        a=the_a;
        b=the_b;
        c=the_c;
    }

    /*public static class NotTwoRoots extends Exception{  //Zero for infinite amount
        public NotTwoRoots() {}
        public NotTwoRoots(int amountofroots) {
        }
    }*/
    public double[] findroots()throws NotTwoRoots{
        double[] roots = new double[2];
        double D = b * b - 4 * a * c;
        if (D >= 0) {
            if(a==0){
                if(b==0){
                    if (c==0) throw new NotTwoRoots(-1);
                    throw new NotTwoRoots(0);

                }
                roots[0]=-c/b;
                roots[1]=roots[0];
            }
            roots[0] = (-b - Math.sqrt(D)) / (2 * a);
            roots[1] = (-b + Math.sqrt(D)) / (2 * a);
            return roots;
        }
        else {
            throw new NotTwoRoots(0);
        }
        /*double D=b*b-4*a*c;
        if(a==0 & b==0 & c==0){
            throw new NotTwoRoots(0);
        }
        if (D<0){
            throw new NotTwoRoots(-1);
        }
        if (a==0){
            if (b==0 & c==0){
                throw new NotTwoRoots(0);
            }

            roots[0]=c/b;
            roots[1]=roots[0];
            return roots;
        }
        roots[0]=(-b-Math.sqrt(D))/(2*a);
        roots[1]=(-b+Math.sqrt(D))/(2*a);

        return roots;*/

    }
}
