import org.junit.Assert;
import org.junit.Test;



public class PolynomTest  {


    @Test//(expected = Exception.class)
 public   void findrootsSuccessTest()  throws Exception {

        Polynom data = new Polynom(1,5,6);
        double[] actual = data.findroots();
        Assert.assertEquals( -3, actual[0],0.001);
        Assert.assertEquals( -2, actual[1],0.001);


    }
    @Test(expected = Exception.class)
 public   void findrootsNoRootsTest()  throws Exception{
        Polynom data = new Polynom(0,0,6);
        double[] actual = data.findroots();
    }
    @Test(expected = Exception.class)
    public   void findrootsInfiniteRootsTest() throws Exception{
        Polynom data = new Polynom(0,0,0);
        double[] actual = data.findroots();
    }


}