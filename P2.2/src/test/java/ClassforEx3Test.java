import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;


public class ClassforEx3Test {
    @Test
    public void findBiggestRootSuccessTest() throws Exception {
        Polynom data = new Polynom(1,5,6);
        double actual=ClassforEx3.findBiggestRoot(data);
        Assert.assertEquals( -2, actual,0.001);
    }
    @Test(expected = NotTwoRoots.class)
    public void findBiggestRootFailTest() throws Exception {
        Polynom data = new Polynom(0,0,0);
        double actual=ClassforEx3.findBiggestRoot(data);

    }


}



