
public class ClassforEx3 {


   public static double findBiggestRoot(Polynom the_polynom) throws NotTwoRoots {
        double result=0;
        double[] buff;
        try {
            buff=the_polynom.findroots();
            result=buff[1];

        } catch (NotTwoRoots ex) {
        throw ex;
        }
        return result;
    }
}
