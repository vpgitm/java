package ru.omsu.imit.polyga.functional;

import org.junit.Assert;
import org.junit.Test;
import ru.omsu.imit.polyga.function.TrigonomitricFunction;
import ru.omsu.imit.polyga.function.WrongConstantsException;
import ru.omsu.imit.polyga.funtional.TrigonomitricFunctionIntegral;


public class TrigonomitricFunctionIntegralTest {
    @Test
    public void getFunctionalTestSuccsess() throws Exception {
        TrigonomitricFunction the_function = new TrigonomitricFunction(1, 1,  0, 2);
        TrigonomitricFunctionIntegral integral = new TrigonomitricFunctionIntegral();
        double actual=integral.getFunctional(the_function);
        Assert.assertEquals(1.41,actual,0.1);
    }
    @Test (expected = WrongConstantsException.class)
    public void getFunctionalTestFail() throws Exception {
        TrigonomitricFunction the_function = new TrigonomitricFunction(2, 0,  666, 2);
        TrigonomitricFunctionIntegral integral = new TrigonomitricFunctionIntegral();
        integral.getFunctional(the_function);
    }
}