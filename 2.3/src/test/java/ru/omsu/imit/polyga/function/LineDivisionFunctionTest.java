package ru.omsu.imit.polyga.function;

import org.junit.Assert;
import ru.omsu.imit.polyga.function.FunctionDomainException;
import ru.omsu.imit.polyga.function.LineDivisionFunction;
import ru.omsu.imit.polyga.function.WrongConstantsException;

public class LineDivisionFunctionTest {

    @org.junit.Test
    public void getLeftBorderTest() throws Exception {
        LineDivisionFunction the_function = new LineDivisionFunction(1,2,3,4,-2,2);
        double actual=the_function.getLeftBorder();
        Assert.assertEquals(-2,actual,0);
    }

    @org.junit.Test
    public void getRightBorderTest() throws Exception {
        LineDivisionFunction the_function = new LineDivisionFunction(1,2,3,4,-2,2);
        double actual=the_function.getRightBorder();
        Assert.assertEquals(2,actual,0);
    }

    @org.junit.Test
    public void getValueTestSuccess() throws Exception {
        LineDivisionFunction the_function = new LineDivisionFunction(1,2,3,4,-2,2);
        double x=2;

        double actual=the_function.getValue(x);

        Assert.assertEquals(0.4,actual,0.001);

    }
    @org.junit.Test
            (expected = FunctionDomainException.class)
    public void getValueTestArgumentNotinDoDException() throws Exception {
        LineDivisionFunction the_function = new LineDivisionFunction(1, 2, 1, 2, -2, 2);
        double x = -2;

        double actual = the_function.getValue(x);
    }
    @org.junit.Test
            (expected = WrongConstantsException.class)
    public void getValueTestWrongConstantsException() throws Exception {
        LineDivisionFunction the_function = new LineDivisionFunction(1, 2, 0, 0, 0, 2);
    }


}