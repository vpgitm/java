package ru.omsu.imit.polyga.functional;

import org.junit.Assert;
import org.junit.Test;
import ru.omsu.imit.polyga.function.LineDivisionFunction;
import ru.omsu.imit.polyga.function.WrongConstantsException;
import ru.omsu.imit.polyga.funtional.LineDivisionFunctionIntegral;


public class LineDivisionFunctionIntegralTest {
    @Test
    public void getFunctionalTestSuccsess() throws Exception {
        LineDivisionFunction the_function = new LineDivisionFunction(2, 0, 0, 2, 0, 2);
        LineDivisionFunctionIntegral integral = new LineDivisionFunctionIntegral();
        double actual=integral.getFunctional(the_function);
        Assert.assertEquals(2,actual,0.1);
    }
    @Test (expected = WrongConstantsException.class)
    public void getFunctionalTestFail() throws Exception {
        LineDivisionFunction the_function = new LineDivisionFunction(2, 0, 0, 0, 0, 2);
        LineDivisionFunctionIntegral integral = new LineDivisionFunctionIntegral();
        integral.getFunctional(the_function);
    }

}