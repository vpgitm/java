package ru.omsu.imit.polyga.functional;

import org.junit.Assert;
import org.junit.Test;
import ru.omsu.imit.polyga.function.LineDivisionFunction;
import ru.omsu.imit.polyga.function.WrongConstantsException;
import ru.omsu.imit.polyga.funtional.FunctionalException;
import ru.omsu.imit.polyga.funtional.LineDivisionFunctionThreePoints;


public class LineDivisionFunctionThreePointsTest {
    @Test
    public void getFunctionalTestSuccsess() throws Exception {
        LineDivisionFunction the_function = new LineDivisionFunction(2, 0, 0, 2, 0, 2);
        LineDivisionFunctionThreePoints threePoints = new LineDivisionFunctionThreePoints();
        double actual=threePoints.getFunctional(the_function);
        Assert.assertEquals(3,actual,0.1);
    }
    @Test (expected = FunctionalException.class)
    public void getFunctionalDomainExceptionTest() throws Exception {
        LineDivisionFunction the_function = new LineDivisionFunction(2, 0, 1, -1, 0, 2);
        LineDivisionFunctionThreePoints threePoints = new LineDivisionFunctionThreePoints();
        threePoints.getFunctional(the_function);
    }
    @Test (expected = WrongConstantsException.class)
    public void getFunctionalWrongConstantsExceptionTest() throws Exception {
        LineDivisionFunction the_function = new LineDivisionFunction(2, 0, 0, 0, 0, 2);
        LineDivisionFunctionThreePoints threePoints = new LineDivisionFunctionThreePoints();
        threePoints.getFunctional(the_function);
    }
}