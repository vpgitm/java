package ru.omsu.imit.polyga.function;

import org.junit.Assert;
import ru.omsu.imit.polyga.function.LineFunction;
import ru.omsu.imit.polyga.function.WrongConstantsException;


public class LineFunctionTest {
    @org.junit.Test
    public void getLeftBorderTest() throws Exception {
        LineFunction the_function = new LineFunction(1,2,-2,2);
        double actual=the_function.getLeftBorder();
        Assert.assertEquals(-2,actual,0);
    }

    @org.junit.Test
    public void getRightBorderTest() throws Exception {
        LineFunction the_function = new LineFunction(1,2,-2,2);
        double actual=the_function.getRightBorder();
        Assert.assertEquals(2,actual,0);
    }
    @org.junit.Test
    public void getValueTestSuccess() throws Exception {
        LineFunction the_function = new LineFunction(1,2,-2,2);
        double x=2;

        double actual=the_function.getValue(x);
        Assert.assertEquals(4,actual,0.001);

    }
    @org.junit.Test
            (expected = WrongConstantsException.class)
    public void getValueTestWrongConstantsException() throws Exception {
        LineFunction the_function = new LineFunction(1, 2,  4, 2);
    }
}