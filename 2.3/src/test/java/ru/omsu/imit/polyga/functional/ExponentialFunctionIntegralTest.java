package ru.omsu.imit.polyga.functional;

import org.junit.Assert;
import org.junit.Test;
import ru.omsu.imit.polyga.function.ExponentialFunction;
import ru.omsu.imit.polyga.function.WrongConstantsException;
import ru.omsu.imit.polyga.funtional.ExponentalFunctionIntegral;


public class ExponentialFunctionIntegralTest {
    @Test
    public void getFunctionalTestSuccsess() throws Exception {
        ExponentialFunction the_function = new ExponentialFunction(1, 1,  0, 2);
        ExponentalFunctionIntegral integral = new ExponentalFunctionIntegral();
        double actual=integral.getFunctional(the_function);
        Assert.assertEquals(8.415,actual,0.1);
    }
    @Test (expected = WrongConstantsException.class)
    public void getFunctionalTestFail() throws Exception {
        ExponentialFunction the_function = new ExponentialFunction(2, 0,  666, 2);
        ExponentalFunctionIntegral integral = new ExponentalFunctionIntegral();
        integral.getFunctional(the_function);
    }
}