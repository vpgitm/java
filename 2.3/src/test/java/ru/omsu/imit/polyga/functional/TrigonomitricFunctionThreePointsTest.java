package ru.omsu.imit.polyga.functional;

import org.junit.Assert;
import org.junit.Test;
import ru.omsu.imit.polyga.function.TrigonomitricFunction;
import ru.omsu.imit.polyga.function.WrongConstantsException;
import ru.omsu.imit.polyga.funtional.TrigonomitricFunctionThreePoints;


public class TrigonomitricFunctionThreePointsTest {
    @Test
    public void getFunctionalTestSuccsess() throws Exception {
        TrigonomitricFunction the_function = new TrigonomitricFunction(2, 0,  0, 2);
        TrigonomitricFunctionThreePoints threePoints = new TrigonomitricFunctionThreePoints();
        double actual=threePoints.getFunctional(the_function);
        Assert.assertEquals(0,actual,0.1);
    }
    @Test (expected = WrongConstantsException.class)
    public void getFunctionalWrongConstantsExceptionTest() throws Exception {
        TrigonomitricFunction the_function = new TrigonomitricFunction(2, 0,  666, 2);
        TrigonomitricFunctionThreePoints threePoints = new TrigonomitricFunctionThreePoints();
        threePoints.getFunctional(the_function);
    }
}