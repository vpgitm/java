package ru.omsu.imit.polyga.functional;

import org.junit.Assert;
import org.junit.Test;
import ru.omsu.imit.polyga.function.LineFunction;
import ru.omsu.imit.polyga.function.WrongConstantsException;
import ru.omsu.imit.polyga.funtional.LineFunctionIntegral;


public class LineFunctionIntegralTest {
    @Test
    public void getFunctionalTestSuccsess() throws Exception {
        LineFunction the_function = new LineFunction(1, 1,  0, 2);
        LineFunctionIntegral integral = new LineFunctionIntegral();
        double actual=integral.getFunctional(the_function);
        Assert.assertEquals(4,actual,0.1);
    }
    @Test (expected = WrongConstantsException.class)
    public void getFunctionalTestFail() throws Exception {
        LineFunction the_function = new LineFunction(2, 0,  666, 2);
        LineFunctionIntegral integral = new LineFunctionIntegral();
        integral.getFunctional(the_function);
    }
}