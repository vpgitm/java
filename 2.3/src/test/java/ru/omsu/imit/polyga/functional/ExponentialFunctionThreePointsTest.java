package ru.omsu.imit.polyga.functional;

import org.junit.Assert;
import org.junit.Test;
import ru.omsu.imit.polyga.function.ExponentialFunction;
import ru.omsu.imit.polyga.function.WrongConstantsException;
import ru.omsu.imit.polyga.funtional.ExponentialFunctionThreePoints;


public class ExponentialFunctionThreePointsTest {
    @Test
    public void getFunctionalTestSuccess() throws Exception {
        ExponentialFunction the_function = new ExponentialFunction(2, 0,  0, 2);
        ExponentialFunctionThreePoints threePoints = new ExponentialFunctionThreePoints();
        double actual=threePoints.getFunctional(the_function);
        Assert.assertEquals(22.21,actual,0.1);
    }
    @Test (expected = WrongConstantsException.class)
    public void getFunctionalWrongConstantsExceptionTest() throws Exception {
        ExponentialFunction the_function = new ExponentialFunction(2, 0,  666, 2);
        ExponentialFunctionThreePoints threePoints = new ExponentialFunctionThreePoints();
        threePoints.getFunctional(the_function);
    }
}