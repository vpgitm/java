package ru.omsu.imit.polyga.functional;

import org.junit.Assert;
import org.junit.Test;
import ru.omsu.imit.polyga.function.LineFunction;
import ru.omsu.imit.polyga.function.WrongConstantsException;
import ru.omsu.imit.polyga.funtional.LineFunctionThreePoints;


public class LineFunctionThreePointsTest {
    @Test
    public void getFunctionalTestSuccsess() throws Exception {
        LineFunction the_function = new  LineFunction(2, 0,  0, 2);
        LineFunctionThreePoints threePoints = new  LineFunctionThreePoints();
        double actual=threePoints.getFunctional(the_function);
        Assert.assertEquals(6,actual,0.1);
    }
    @Test (expected = WrongConstantsException.class)
    public void getFunctionalWrongConstantsExceptionTest() throws Exception {
        LineFunction the_function = new  LineFunction(2, 0,  666, 2);
        LineFunctionThreePoints threePoints = new  LineFunctionThreePoints();
        threePoints.getFunctional(the_function);
    }
}