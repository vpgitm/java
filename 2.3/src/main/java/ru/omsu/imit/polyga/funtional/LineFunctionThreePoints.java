package ru.omsu.imit.polyga.funtional;

import ru.omsu.imit.polyga.function.LineFunction;

public class LineFunctionThreePoints extends ThreePoints<LineFunction> {
    public double getFunctional(LineFunction the_function){
        double result=0;
        result+=the_function.getValue(the_function.getLeftBorder());
        result+=the_function.getValue(the_function.getRightBorder());
        result+=the_function.getValue((the_function.getLeftBorder()+the_function.getRightBorder())/2);

        return result;
    }
}

