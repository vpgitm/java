package ru.omsu.imit.polyga.function;

public class TrigonomitricFunction implements OneArgumentFunction{
    private final double leftBorder;
    private final double rightBorder;
    private final double A;
    private final double B;
    public TrigonomitricFunction(double the_A, double the_B,double the_leftBorder, double the_rightBorder) throws WrongConstantsException {
        leftBorder= the_leftBorder;
        rightBorder=the_rightBorder;
        A=the_A;
        B=the_B;
        if(leftBorder>rightBorder){
            throw new WrongConstantsException();
        }
    }

    public double getLeftBorder() {
        return leftBorder;
    }

    public double getRightBorder() {
        return rightBorder;
    }

    public double getValue(double x) {
        return A*Math.sin(B*x);
    }
}
