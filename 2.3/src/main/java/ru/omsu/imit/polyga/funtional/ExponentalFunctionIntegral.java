package ru.omsu.imit.polyga.funtional;

import ru.omsu.imit.polyga.function.ExponentialFunction;

public class ExponentalFunctionIntegral extends Integral<ExponentialFunction> {
    public double getFunctional(ExponentialFunction the_function) {
        double delta=0.005;
        double result=0;
        double rightBorder=the_function.getRightBorder();
        for(double i=the_function.getLeftBorder();i<rightBorder;i+=delta){
            result+=the_function.getValue(i)*delta;
        }
        return result;
    }
}
