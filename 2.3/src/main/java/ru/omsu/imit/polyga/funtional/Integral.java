package ru.omsu.imit.polyga.funtional;

public abstract class Integral<T> implements OneArgumentFunctional<T> {
    public abstract double getFunctional(T the_function) throws FunctionalException;
}
