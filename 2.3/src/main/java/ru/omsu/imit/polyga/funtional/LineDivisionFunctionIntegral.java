package ru.omsu.imit.polyga.funtional;

import ru.omsu.imit.polyga.function.FunctionDomainException;
import ru.omsu.imit.polyga.function.LineDivisionFunction;

public class LineDivisionFunctionIntegral extends Integral<LineDivisionFunction> {
    public double getFunctional(LineDivisionFunction the_function) throws FunctionalException {
        double delta=0.005;
        double result=0;
        double rightBorder=the_function.getRightBorder();
        for(double i=the_function.getLeftBorder();i<rightBorder;i+=delta){
            try{
                result+=the_function.getValue(i)*delta;
            }
            catch (FunctionDomainException ex){
                try{
                result+=the_function.getValue(i-delta)*delta;
                }catch (FunctionDomainException ex2){throw new FunctionalException();}
            }

        }
        return result;
    }
}
