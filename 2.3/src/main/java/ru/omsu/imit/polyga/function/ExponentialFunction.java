package ru.omsu.imit.polyga.function;

public class ExponentialFunction {
    private double leftBorder;
    private double rightBorder;
    private double A;
    private double B;
    public ExponentialFunction(double the_A, double the_B, double the_leftBorder, double the_rightBorder) throws WrongConstantsException {
        leftBorder= the_leftBorder;
        rightBorder=the_rightBorder;
        A=the_A;
        B=the_B;
        if(leftBorder>rightBorder){
            throw new WrongConstantsException();
        }
    }

    public double getLeftBorder() {
        return leftBorder;
    }

    public double getRightBorder() {
        return rightBorder;
    }

    public double getValue(double x) {
        return A*Math.exp(x)+B;
    }
}
