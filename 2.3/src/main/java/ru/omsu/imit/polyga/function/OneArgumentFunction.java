package ru.omsu.imit.polyga.function;

public interface OneArgumentFunction {
    double getLeftBorder();
    double getRightBorder();
    double getValue(double x);
}
