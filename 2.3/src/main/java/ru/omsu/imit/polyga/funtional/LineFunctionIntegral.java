package ru.omsu.imit.polyga.funtional;

import ru.omsu.imit.polyga.function.LineFunction;

public class LineFunctionIntegral extends Integral<LineFunction> {
    final double delta;
    public LineFunctionIntegral(){
        delta=0.005;
    }
    public LineFunctionIntegral(double delta){
        this.delta=delta;
    }
    public double getFunctional(LineFunction the_function) {

        double result=0;
        double rightBorder=the_function.getRightBorder();
        for(double i=the_function.getLeftBorder();i<rightBorder;i+=delta){
            result+=the_function.getValue(i)*delta;
        }
        return result;
    }
}
