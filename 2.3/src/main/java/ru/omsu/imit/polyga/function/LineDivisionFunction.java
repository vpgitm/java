package ru.omsu.imit.polyga.function;

public class LineDivisionFunction {
    private final double leftBorder;
    private final double rightBorder;
    private final double A;
    private final double B;
    private final double C;
    private final double D;
    public LineDivisionFunction(double the_A, double the_B, double the_C, double the_D, double the_leftBorder, double the_rightBorder) throws WrongConstantsException {
        leftBorder= the_leftBorder;
        rightBorder=the_rightBorder;
        A=the_A;
        B=the_B;
        C=the_C;
        D=the_D;
        if(C==0 & D==0){
            throw new WrongConstantsException();
        }
        if(leftBorder>rightBorder){
            throw new WrongConstantsException();
        }
    }

    public double getLeftBorder() {
        return leftBorder;
    }

    public double getRightBorder() {
        return rightBorder;
    }

    public double getValue(double x) throws FunctionDomainException {
        if(C*x+D==0) throw new FunctionDomainException();
        double result =(A*x+B)/(C*x+D);
        return result;
    }
}
