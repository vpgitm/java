package ru.omsu.imit.polyga.funtional;

import ru.omsu.imit.polyga.function.FunctionDomainException;
import ru.omsu.imit.polyga.function.LineDivisionFunction;

public class LineDivisionFunctionThreePoints extends ThreePoints<LineDivisionFunction> {
    public double getFunctional(LineDivisionFunction the_function) throws FunctionalException {
        double result=0;
        try {
            result += the_function.getValue(the_function.getLeftBorder());
            result += the_function.getValue(the_function.getRightBorder());
            result += the_function.getValue((the_function.getLeftBorder() + the_function.getRightBorder()) / 2);
        }
        catch(FunctionDomainException ex){
            throw new FunctionalException();
        }
        return result;
    }
}
