package ru.omsu.imit.polyga.funtional;

import ru.omsu.imit.polyga.function.TrigonomitricFunction;

public class TrigonomitricFunctionThreePoints extends ThreePoints<TrigonomitricFunction> {
    public double getFunctional(TrigonomitricFunction the_function){
        double result=0;
        result+=the_function.getValue(the_function.getLeftBorder());
        result+=the_function.getValue(the_function.getRightBorder());
        result+=the_function.getValue((the_function.getLeftBorder()+the_function.getRightBorder())/2);
        return result;
    }
}
