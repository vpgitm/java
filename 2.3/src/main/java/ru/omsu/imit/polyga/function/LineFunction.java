package ru.omsu.imit.polyga.function;

public class LineFunction implements OneArgumentFunction {
    private final double leftBorder;
    private final double rightBorder;
    private final double A;
    private final double B;
    public LineFunction(double the_A, double the_B,double the_leftBorder, double the_rightBorder) throws WrongConstantsException {
        leftBorder= the_leftBorder;
        rightBorder=the_rightBorder;
        A=the_A;
        B=the_B;
        if(leftBorder>rightBorder){
            throw new WrongConstantsException();
        }
    }
    public double getLeftBorder() {
        return leftBorder;

    }

    public double getRightBorder() {
        return rightBorder;
    }

    public double getValue(double x) {
        return A*x+B;
    }
    /*
    public double getFunctional() {
        double delta=0.005;
        double result=0;
        for(double i=leftBorder;i<rightBorder;i+=delta){
            result+=getValue(i)*delta;
        }
        return result;
    }

    public double getFunctional(double delta) {
        double result=0;
        for(double i=leftBorder;i<rightBorder;i+=delta){
            result+=getValue(i)*delta;
        }
        return result;
    }
    */
}
