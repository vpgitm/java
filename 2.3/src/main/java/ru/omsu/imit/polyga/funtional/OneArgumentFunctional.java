package ru.omsu.imit.polyga.funtional;

public interface OneArgumentFunctional<T> {
   double getFunctional(T function) throws FunctionalException;
}
