package ru.omsu.imit.polyga.matrix;

public interface IMatrix {
    void set(int i, int j, double value) throws ZeroDeterminantException;
    double get(int i, int j);
    int getSize();
    double determinant();
}
