package ru.omsu.imit.polyga.matrix;

import ru.omsu.imit.polyga.invertiblematrix.*;

import java.util.InputMismatchException;
import java.util.Scanner;
import java.io.*;

public class DemoMatrix {
    private static int sizeMax=100;
    private static int sizeofbuff=10000;

    private static void printMatrix(IMatrix theMatrix){
        int size =  theMatrix.getSize();
        double x;
        for(int i=0;  i<size; i++){
            for(int j=0;  j<size; j++){
                x=theMatrix.get(i, j);
                System.out.printf("%f ", x);
            }
            System.out.println();
        }
    }
    public static double Sum(IMatrix Matrix){
        double result=0;
        for(int i=0;  i<Matrix.getSize(); i++){
            for(int j=0;  j<Matrix.getSize(); j++){
                result+=Matrix.get(i,j);
            }
        }
        return result;
    }

    public static Matrix readMatrixfromFileDis(String place) throws IOException, ReadException {
        try (DataInputStream dis = new DataInputStream(new FileInputStream(new File(place)))) {
            double[] nums = new double[sizeofbuff];
            int size;
            int numberofElements=0;
            try{
            while(true){
                if(sizeofbuff<numberofElements){
                    throw new BufferOverflowException();
                }
                nums[numberofElements]=dis.readDouble();
                numberofElements++;
            }
            }catch (EOFException ex){}
            size = (int) Math.sqrt( numberofElements );
            if(size*size!=numberofElements){
                throw new WrongSizeException();
            }
            if(size<1 || size>sizeMax){
                throw new WrongSizeException();
            }
            Matrix theMatrix=new Matrix(size);
            for(int i=0; i<size; i++){
                for(int j=0; j<size; j++){
                    theMatrix.set(i,j, nums[i*size+j]);
                }
            }
            return theMatrix;
        }
    }

    private static void writeMatrixToFileDos(String place, IMatrix theMatrix) throws IOException {
        try (DataOutputStream dos = new DataOutputStream(new FileOutputStream(new File(place))))
        {
            int size=theMatrix.getSize();
            for (int i=0; i<size; i++) {
                for (int j = 0; j < size; j++) {
                    dos.writeDouble(theMatrix.get(i,j));
                }
            }
        }
    }

    private static int readANumber(Scanner in){   //Создает сканнер каждый раз Решено?
        int x=0;
        //Scanner in = new Scanner(System.in);
        boolean isNumber=false;
        while(!isNumber) {
            try {
                x = in.nextInt();
                isNumber=true;
                in.nextLine();
            }
            catch (InputMismatchException ex) {
                System.out.printf("Это не целое число, введите заново");
                in.nextLine();
            }
        }

        return x;
    }

    private static Matrix readMatrix() {
        Matrix theMatrix = null;
        Scanner in = new Scanner(System.in);
        int size = 0;
        double elem = 0;
        System.out.println("Введите размер матрицы");
        size = readANumber(in);

        while (size < 1 || size > sizeMax) {
            System.out.printf("Размер некорректен, или превышает максимальное значение (%d). Введите заново.%n", sizeMax);
            size = readANumber(in);
        }
        //Scanner in = new Scanner(System.in);
        boolean isNumber = false;
        theMatrix = new Matrix(size);
        System.out.printf("Введите матрицу %n");
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                while (!isNumber) {
                    try {
                        elem = in.nextDouble();
                        isNumber = true;
                        theMatrix.set(i,j, elem);
                    } catch (InputMismatchException ex) {
                        System.out.printf("Это не целое число, введите заново%n");
                        in.nextLine();
                    }
                }
                isNumber = false;
            }
        }
        in.close();
        return theMatrix;
    }
    public static Matrix Multiply(IMatrix first,IMatrix second) throws NotSquareMatrix {
        if(first.getSize()!=second.getSize())
            throw new NotSquareMatrix();
        Matrix result=new Matrix(first.getSize());
        double buff=0;
        for (int i=0;i<first.getSize();i++){
            for (int j=0;j<first.getSize();j++){
                buff=0;
                for (int r=0;r<first.getSize();r++){
                    buff+=first.get(i,r)*second.get(r,j);
                }
                result.set(i,j,buff);
            }
        }
        return result;
    }
  /* public Matrix readMatrix() throws NotSquareMatrix{
       Scanner in = new Scanner(System.in);
       int numberofelems=0;
       int size=0;
       double[] nums = new double[sizeofbuff];
       while(in.hasNextDouble()==true){
           nums[size]=in.nextDouble();
           size++;
       }
       Matrix theMatrix=new Matrix(size);
       for(int i=0;i < size; i++){
           theMatrix.set(i,0, nums[i]);
       }
       for(int i=1; i<size; i++){
           //Scanner in = new Scanner(System.in);
           for(int j=0; j<size; j++){

           }
       }
        return theMatrix;
   }*/
    public static void main(String[] args) {
        IMatrix theMatrix;
        theMatrix=readMatrix();
        IInvertibleMatrix theInvertedMatrix;
        printMatrix(theMatrix);
        System.out.printf("Сумма элементов %f", Sum(theMatrix));
        System.out.printf("Определитель %f %n", theMatrix.determinant());
        try {
            InvertibleMatrix theInvertibleMatrix=new InvertibleMatrix(theMatrix);
            printMatrix(theInvertedMatrix=theInvertibleMatrix.InvertMatrix());
            System.out.printf("Проверка%n");
            printMatrix(Multiply(theMatrix,theInvertedMatrix));
        }
        catch (ZeroDeterminantException ex){
            System.out.printf("Матрица не инвертируемая%n");
        } catch (NotSquareMatrix notSquareMatrix) {
            System.out.printf("Матрица не квадратная%n");
        }


        try {
            writeMatrixToFileDos("matrix" ,theMatrix);
            System.out.printf("Запись%n");
        } catch (IOException e) {
            System.out.printf("Ошибка чтения%n");
        }
        try {
            theMatrix = readMatrixfromFileDis("matrix");
            System.out.printf("Чтение%n");
        } catch (IOException e) {
            System.out.printf("Ошибка чтения");
        } catch (ReadException e) {
            System.out.printf("Файл не является матрицей");
        }
        printMatrix(theMatrix);
        FileOutputStream fos;
        try {
            fos = new FileOutputStream("matrix");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(theMatrix);
            oos.flush();
            System.out.printf("Сериализация%n");
        } catch (IOException e) {
            System.out.printf("Ошибка%n");
        }
        FileInputStream fis;
        try {
            fis = new FileInputStream("matrix");
            ObjectInputStream oin = null;
            oin = new ObjectInputStream(fis);
            Matrix ts = (Matrix) oin.readObject();
            System.out.printf("Десериализация%n");
        } catch (ClassNotFoundException | IOException e) {
            System.out.printf("Ошибка%n");
        }

        printMatrix(theMatrix);



    }

}
