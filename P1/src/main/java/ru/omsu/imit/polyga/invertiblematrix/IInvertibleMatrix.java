package ru.omsu.imit.polyga.invertiblematrix;

import ru.omsu.imit.polyga.matrix.IMatrix;

public interface IInvertibleMatrix extends IMatrix {
    InvertibleMatrix InvertMatrix();
}
