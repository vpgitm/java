package ru.omsu.imit.polyga.invertiblematrix;

import ru.omsu.imit.polyga.matrix.IMatrix;
import ru.omsu.imit.polyga.matrix.Matrix;
import ru.omsu.imit.polyga.matrix.ZeroDeterminantException;

import java.io.Serializable;

public class InvertibleMatrix implements IInvertibleMatrix, Serializable {
    int sizeofmatrix;
    double[] matrix=null;
    public InvertibleMatrix(int size) {
        sizeofmatrix=size;
        matrix = new double[size*size];
        for(int i=0; i<sizeofmatrix;i++) {
            for(int j=0; j<sizeofmatrix;j++) {
                matrix[i*sizeofmatrix+j]=0;

            }
        }
        matrix = new double[size*size];
        for(int i=0; i<sizeofmatrix;i++) {
            matrix[i*sizeofmatrix+i]=1;
        }
    }

    public double get(int i, int j){
        return matrix[i*sizeofmatrix+j];
    }

    public double determinant() {
        double EPS = 1E-9;
        double det=1;
        InvertibleMatrix matr = new InvertibleMatrix(this);
        double buff;
        for (int i=0; i<sizeofmatrix; i++){
            int k = i;
            for (int j=i+1; j<sizeofmatrix; ++j) {
                if (Math.abs(matr.get(k, i)) > Math.abs(matr.get(k, i))) {
                    k = j;
                }
            }
            if (Math.abs(matr.get(k, i)) < EPS)
            {
                return 0;
            }
            for(int m=0; m<sizeofmatrix; m++){
                buff = matr.matrix[i*sizeofmatrix+m];
                matr.matrix[i*sizeofmatrix+m] = matr.matrix[k*sizeofmatrix+m];   //!!!
                matr.matrix[k*sizeofmatrix+m] = buff;
            }
            if (i != k){
                det = -det;
            }
            det =det * matr.matrix[i*sizeofmatrix+i];
            for (int j=i+1; j<sizeofmatrix; ++j){
                matr.matrix[i*sizeofmatrix+j] = matr.matrix[i*sizeofmatrix+j]/matr.matrix[i*sizeofmatrix+i];
            }
            for (int j=0; j<sizeofmatrix; ++j){
                if ((j != i)&&(Math.abs(matr.matrix[j*sizeofmatrix+i]) > EPS)){
                    for (k = i+1; k < sizeofmatrix; ++k){
                        matr.matrix[j*sizeofmatrix+k] -= matr.matrix[i*sizeofmatrix+k] * matr.matrix[j*sizeofmatrix+i];
                    }
                }
            }
        }
        return det;
    }
    public int getSize() {
        return sizeofmatrix;
    }

    public InvertibleMatrix(Matrix matr) throws ZeroDeterminantException {
        sizeofmatrix=matr.getSize();
        matrix = new double[sizeofmatrix*sizeofmatrix];
        for(int i=0; i<sizeofmatrix;i++) {
            for(int j=0; j<sizeofmatrix;j++) {
                matrix[i*sizeofmatrix+j]=matr.get(i,j);
            }
        }
        if(matr.determinant()==0){
            throw new ZeroDeterminantException();
        }
    }
    public InvertibleMatrix(IInvertibleMatrix matr) {
        sizeofmatrix=matr.getSize();
        matrix = new double[sizeofmatrix*sizeofmatrix];
        for(int i=0; i<sizeofmatrix;i++) {
            for(int j=0; j<sizeofmatrix;j++) {
                matrix[i*sizeofmatrix+j]=matr.get(i,j);
            }
        }
    }
    public InvertibleMatrix(IMatrix matr) throws ZeroDeterminantException {
        sizeofmatrix=matr.getSize();
        matrix = new double[sizeofmatrix*sizeofmatrix];
        for(int i=0; i<sizeofmatrix;i++) {
            for(int j=0; j<sizeofmatrix;j++) {
                matrix[i*sizeofmatrix+j]=matr.get(i,j);
            }
        }
        if(this.determinant()==0){
            throw new ZeroDeterminantException();
        }
    }

    protected void swapLines(int x, int y){
        double buff=0;
        for (int m = 0; m < sizeofmatrix; m++) {
            buff = matrix[x * sizeofmatrix + m];
            matrix[x * sizeofmatrix + m] = matrix[y * sizeofmatrix + m];
            matrix[y * sizeofmatrix + m] = buff;
        }
    }

    protected void divideLine(int i, double x){
        for (int m = 0; m < sizeofmatrix; m++){
            matrix[i * sizeofmatrix + m] = matrix[i * sizeofmatrix + m] / x;
        }
    }

    protected void addMutlipliedLine(int i, int j, double x){
        //double buff=0;
        for (int m = 0; m < sizeofmatrix; m++) {
            matrix[i * sizeofmatrix + m] = matrix[j * sizeofmatrix + m]*x + matrix[i * sizeofmatrix + m];
        }
    }

    public InvertibleMatrix InvertMatrix() {
        InvertibleMatrix temp= null;
        temp = new InvertibleMatrix(this);
        InvertibleMatrix result=new InvertibleMatrix(sizeofmatrix);
        double buff=0;
        for(int i=0;i<sizeofmatrix;i++){
            result.matrix[i*sizeofmatrix+i]=1;
        }
        //первый проход
        //находим строку с ненулевым и-ым элементом
        for(int i=0; i<sizeofmatrix; i++){
                int k = i;
                buff = temp.matrix[i * sizeofmatrix + i];
                while (buff == 0) {
                    k++;
                    buff = temp.matrix[k * sizeofmatrix + i];
                }
            temp.divideLine(k, buff);
            result.divideLine(k, buff);
                //меняем строки
                if (k != i) {
                   temp.swapLines(i, k);
                   result.swapLines(i,k);
                }
            //зануляем все что ниже i i
            for(int j=i+1; j<sizeofmatrix; j++){
                buff=-1*temp.matrix[j * sizeofmatrix + i];
                temp.addMutlipliedLine(j, i, buff);
                result.addMutlipliedLine(j, i, buff);
            }

        }
        //второй проход
        for(int i=sizeofmatrix-1; i>=0; i--){
            for(int j=i-1; j>=0; j--){
                buff=-1*temp.matrix[j * sizeofmatrix + i];
                temp.addMutlipliedLine(j, i, buff);
                result.addMutlipliedLine(j, i, buff);
            }
        }

        return result;
    }

    @Override
    public void set(int i, int j, double value) throws ZeroDeterminantException {
        double buff = this.get(i,j);
        matrix[i*sizeofmatrix+j]=value;
        if (this.determinant()==0){
            matrix[i*sizeofmatrix+j]=value;
        throw new ZeroDeterminantException();
        }
    }
}
