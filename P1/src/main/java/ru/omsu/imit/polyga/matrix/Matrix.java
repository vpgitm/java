package ru.omsu.imit.polyga.matrix;

import java.util.Arrays;
import java.io.Serializable;


public class Matrix implements IMatrix, Serializable {
    int sizeofmatrix;
    double[] matrix=null;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Matrix matrix = (Matrix) o;

        if (sizeofmatrix != matrix.sizeofmatrix) return false;
        return Arrays.equals(this.matrix, matrix.matrix);
    }

    @Override
    public int hashCode() {
        int result = sizeofmatrix;
        result = 31 * result + Arrays.hashCode(matrix);
        return result;
    }

    protected void swapLines(int x, int y){
        double buff=0;
        for (int m = 0; m < sizeofmatrix; m++) {
            buff = matrix[x * sizeofmatrix + m];
            matrix[x * sizeofmatrix + m] = matrix[y * sizeofmatrix + m];
            matrix[y * sizeofmatrix + m] = buff;
        }
    }

    protected void divideLine(int i, double x){
        for (int m = 0; m < sizeofmatrix; m++){
            matrix[i * sizeofmatrix + m] = matrix[i * sizeofmatrix + m] / x;
        }
    }

    protected void addMutlipliedLine(int i, int j, double x){
        //double buff=0;
        for (int m = 0; m < sizeofmatrix; m++) {
            matrix[i * sizeofmatrix + m] = matrix[j * sizeofmatrix + m]*x + matrix[i * sizeofmatrix + m];
        }
    }

    public Matrix(int size){
       sizeofmatrix=size;
        matrix = new double[size*size];
        for(int i=0; i<sizeofmatrix;i++) {
            for(int j=0; j<sizeofmatrix;j++) {
                matrix[i*sizeofmatrix+j]=0;

            }
        }

    }

    public Matrix(Matrix theMatrix){
        sizeofmatrix=theMatrix.sizeofmatrix;
        matrix = new double[sizeofmatrix*sizeofmatrix];
        for(int i=0; i<sizeofmatrix;i++) {
            for(int j=0; j<sizeofmatrix;j++) {
                matrix[i*sizeofmatrix+j]=theMatrix.matrix[i*sizeofmatrix+j];
            }
        }
    }

    public void set(int i, int j, double value) {
        matrix[i*sizeofmatrix+j]=value;
    }

    public double get(int i, int j){
        return matrix[i*sizeofmatrix+j];
    }

    public int getSize() {
        return sizeofmatrix;
    }

    public double determinant() {
        double EPS = 1E-9;
        double det=1;
        Matrix matr = new Matrix(this);
        double buff;
        for (int i=0; i<sizeofmatrix; i++){
            int k = i;
            for (int j=i+1; j<sizeofmatrix; ++j) {
                if (Math.abs(matr.get(k, i)) > Math.abs(matr.get(k, i))) {
                    k = j;
                }
            }
            if (Math.abs(matr.get(k, i)) < EPS)
            {
                return 0;
            }
            for(int m=0; m<sizeofmatrix; m++){
                buff = matr.matrix[i*sizeofmatrix+m];
                matr.matrix[i*sizeofmatrix+m] = matr.matrix[k*sizeofmatrix+m];   //!!!
                matr.matrix[k*sizeofmatrix+m] = buff;
            }
            if (i != k){
                det = -det;
            }
            det =det * matr.matrix[i*sizeofmatrix+i];
            for (int j=i+1; j<sizeofmatrix; ++j){
                matr.matrix[i*sizeofmatrix+j] = matr.matrix[i*sizeofmatrix+j]/matr.matrix[i*sizeofmatrix+i];
            }
            for (int j=0; j<sizeofmatrix; ++j){
                if ((j != i)&&(Math.abs(matr.matrix[j*sizeofmatrix+i]) > EPS)){
                    for (k = i+1; k < sizeofmatrix; ++k){
                        matr.matrix[j*sizeofmatrix+k] -= matr.matrix[i*sizeofmatrix+k] * matr.matrix[j*sizeofmatrix+i];
                    }
                }
            }
        }
        return det;
    }


}
